import { TObject } from "../interfaces";
import { getStorage, setStorage, deleteStorage } from "./common";

const clearStorage = (): void => {
  deleteStorage("PermissionArr");
  deleteStorage("LoginInfo");
};

const saveStorage = (data: TObject): void => {
  setStorage("PermissionArr", data.permission_arr || []);
  setStorage("LoginInfo", { hashId: data.hash_id, email: data.email });
};

export const check = (access: string, ability: number): boolean => {
  // ability = [1: creact, 2: read, 4: update, 8:delete]
  const permissionArr: Array<TObject> = getStorage("PermissionArr") || [];
  const permission = permissionArr.filter(
    (permission) => access === permission.access
  );

  return permission[0] && (permission[0].ability & ability) === ability;
};

export default {
  saveStorage: saveStorage,
  clearStorage,
  check,
};
