import { toast } from "react-toastify";

import { TObject, TRespHandler, TDispatch } from "../interfaces";
import t from "../utils/translation";

const handleResp = (
  dispatch: TDispatch,
  resp: TObject,
  respHandler: TRespHandler,
  ...rest: any
) => {
  if (resp.status === 401) {
    resp.data && toast.error(t(resp.data.Msg));
    loginForm(dispatch);
  } else {
    if (typeof respHandler[resp.status] === "function") {
      respHandler[resp.status](resp.data, ...rest);
    } else if (typeof respHandler.else === "function") {
      respHandler.else(resp.data, ...rest);
    } else {
      console.log(resp);
      console.log(resp.status);
      resp.data && toast.error(t(resp.data.Msg));
    }
  }
  if (typeof respHandler.finally === "function") {
    respHandler.finally(resp.data, ...rest);
  }
};

const loginForm = (dispatch: TDispatch) => {
  dispatch({
    type: "dialog",
    payload: {
      show: true,
      type: "loginForm",
    },
  });
};

export default {
  handleResp,
  loginForm,
};
