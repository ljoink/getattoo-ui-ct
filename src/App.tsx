import "react-app-polyfill/stable";

import React from "react";
import { BrowserRouter } from "react-router-dom";

import { StoreProvider } from "./Store";
import Initialization from "./components/Initialization";

import Main from "./components/Main";
import { TObject } from "./interfaces";

import "@fortawesome/fontawesome-free/css/all.min.css";
import "./style/main.scss";

export default function App(props: TObject): JSX.Element {
  return (
    <StoreProvider {...props}>
      <BrowserRouter>
        <Initialization />
        <Main />
      </BrowserRouter>
    </StoreProvider>
  );
}
