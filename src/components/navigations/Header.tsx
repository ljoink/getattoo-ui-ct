import React from "react";
import { Link } from "react-router-dom";
import { toast } from "react-toastify";

import { Store } from "../../Store";
import { getLangUrl } from "../../utils/common";
import t from "../../utils/translation";
import LanguageDropdown from "./LanguageDropdown";
import Membership from "./Membership";

import Logo from "../Logo";
import "../../style/header.scss";

export default function Header(): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const { header: headerS } = state;
  const dispatchType = "header";

  const toggleCollapse = (): void => {
    headerS.collapsed = !headerS.collapsed;
    dispatch({ type: dispatchType, payload: headerS });
  };

  const alertDevloping = () => {
    toast.warn(t("Developing") + "...");
  };

  return (
    // <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
    <nav className="navbar navbar-expand-lg fixed-top">
      <Link className="navbar-brand" to={getLangUrl()}>
        <Logo />
      </Link>

      <button
        className="navbar-toggler btn-sm"
        type="button"
        onClick={toggleCollapse}
      >
        <div className={`hamburger${headerS.collapsed ? "" : " open"}`}>
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
      </button>

      <div className={`navbar-collapse ${headerS.collapsed ? "collapse" : ""}`}>
        <ul className="navbar-nav mr-auto">
          <li className="nav-item" onClick={alertDevloping}>
            <div className="nav-link">{t("Appointment")}</div>
          </li>
        </ul>

        <form className="form-inline my-2 my-lg-0">
          <i onClick={alertDevloping} className="fa fa-search text-success" />
          &nbsp;
          <input
            className="form-control mr-sm-2"
            type="text"
            placeholder={t("Search")}
          ></input>
        </form>

        <ul className="navbar-nav ">
          <LanguageDropdown />
          <li className="nav-item" onClick={alertDevloping}>
            <div className="nav-link">
              <i className="fa fa-heart" />
              <span className="badge-count">5</span>
            </div>
          </li>
          <li className="nav-item pr-3" onClick={alertDevloping}>
            <div className="nav-link wrapper">
              <i className="fa fa-shopping-cart" />
            </div>
          </li>

          <li className="nav-item">
            <Membership />
          </li>
        </ul>
      </div>
    </nav>
  );
}
