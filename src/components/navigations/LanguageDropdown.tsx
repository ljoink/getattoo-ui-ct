import React from "react";

import { Store } from "../../Store";
import { setStorage, getStorage } from "../../utils/common";

export default function LanguageDropdown(): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const { header: headerS, languageArr: languageArrS } = state;
  const isExpanded = headerS.expandedLanguageArr;

  const toggleSettingLanguage = (): void => {
    headerS.expandedLanguageArr = !isExpanded;

    dispatch({
      type: "header",
      payload: headerS,
    });
  };

  const changeLang = (code: string): void => {
    setStorage("Lang", code);

    headerS.expandedLanguageArr = false;
    dispatch({
      type: "header",
      payload: headerS,
    });
  };

  return (
    <li
      className={`nav-item dropdown ${isExpanded && "active show"}`}
      onClick={toggleSettingLanguage}
    >
      <div className="nav-link">
        <i className="fa fa-globe" />
        &nbsp;
        {
          languageArrS.filter(
            (language) => language.code === getStorage("Lang")
          )[0].name
        }
        &nbsp;
        <i className="fa fa-angle-left" />
      </div>
      <div className={`dropdown-menu ${isExpanded && "show"}`}>
        {languageArrS.map((current) => {
          if (current.code === getStorage("Lang")) {
            return null;
          }

          return (
            <div
              className="dropdown-item"
              key={current.code}
              onClick={() => {
                changeLang(current.code);
              }}
            >
              <i className="far fa-circle fa-xs" />
              &nbsp;
              {current.name}
            </div>
          );
        })}
      </div>
    </li>
  );
}
