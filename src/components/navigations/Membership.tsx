import React from "react";
import { toast } from "react-toastify";

import { Store } from "../../Store";
import { TObject } from "../../interfaces";
import { getLangUrl, getStorage, getEndpoint } from "../../utils/common";
import http from "../../utils/http";
import hook from "../../utils/hook";
import auth from "../../utils/auth";
import t from "../../utils/translation";

export default function Membership(): JSX.Element {
  const { dispatch } = React.useContext(Store);

  const doLogout = async (): Promise<void> => {
    dispatch({ type: "dialogMask" });

    const config: TObject = http.defaultConfig;
    const csrf = await http.getCsrfToken();
    http.handleCsrf(csrf, config);

    const { data, status } = await http.get(
      `${getEndpoint()}guest/logout`,
      config
    );
    if (status === 200) {
      auth.clearStorage();
      document.location.href = `${getLangUrl()}`;
    } else {
      toast.error(data.Msg);
    }
  };

  const handleLogOut = () => {
    dispatch({
      type: "dialog",
      payload: {
        show: true,
        title: "Do you logout?",
        type: "confirm",
        onConfirm: doLogout,
      },
    });
  };

  return (
    <>
      {getStorage("PermissionArr") ? (
        <button
          className="btn btn-outline-success my-2 my-sm-0"
          type="submit"
          onClick={() => {
            handleLogOut();
          }}
        >
          {t("Logout")}
        </button>
      ) : (
        <button
          className="btn btn-outline-success my-2 my-sm-0"
          type="submit"
          onClick={() => {
            hook.loginForm(dispatch);
          }}
        >
          {t("Login")}
        </button>
      )}
    </>
  );
}
