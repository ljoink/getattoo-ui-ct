import React from "react";
import ImageGallery from "react-image-gallery";

import { Store } from "../../Store";
import { getEndpoint, getStorage } from "../../utils/common";

import "react-image-gallery/styles/css/image-gallery.css";

export default function Gallery({
  dispatchType: dispatchTypeP,
}: any): JSX.Element {
  const { state } = React.useContext(Store);
  const galleryS = state[dispatchTypeP] || [];

  const items = galleryS.map((current: any) => {
    return {
      original: `${getEndpoint()}${current.path}/${current.photo}`,
      thumbnail: `${getEndpoint()}${current.path}/thumb/${current.photo}`,
      description: current["desc_" + getStorage("Lang")],
    };
  });

  return (
    <ImageGallery
      items={items}
      showThumbnails={false}
      showBullets={true}
      showPlayButton={false}
      showFullscreenButton={false}
      autoPlay={true}
    />
  );
}
