import React from "react";
import { Route } from "react-router-dom";

import { TObject } from "../../interfaces";

export const AuthRoute = ({
  component: Component,
  lazy: Lazy,
  ...rest
}: TObject): JSX.Element => {
  return (
    <Route
      {...rest}
      render={() => (
        <>
          {Component ? (
            <Component />
          ) : (
            <React.Suspense fallback="">
              <Lazy />
            </React.Suspense>
          )}
        </>
      )}
    />
  );
};
