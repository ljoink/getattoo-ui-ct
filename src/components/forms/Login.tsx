import React from "react";
import { toast } from "react-toastify";

import { Store } from "../../Store";
import { IForm, IFormField } from "../../interfaces";
import auth from "../../utils/auth";
import t from "../../utils/translation";

import Form from "../common/Form";

export default function Login(): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const dispatchType = "loginForm";

  const dialogS = state.dialog || {};
  const loginFormS = state.loginForm || {};
  loginFormS.data = loginFormS.data || {};

  const fields: Array<IFormField> = [
    {
      name: "email",
      label: "Email address",
      className: "col-sm-12 floating-label",
      schema: "email;required",
    },
    {
      name: "password",
      type: "password",
      label: "Password",
      className: "col-sm-12 floating-label",
      schema: "required",
    },
  ];

  const respHandler = {
    "200": (data?: any): void => {
      if (data) {
        auth.saveStorage(data);
        dispatch({ type: "dialogClose" });
        if (typeof dialogS.callBack === "function") {
          dialogS.callBack(dialogS.callBack);
        }

        toast(t("Welcome Back"));
        dispatch({
          type: "delete",
          payload: dispatchType,
        });
      }
    },
    451: (): void => {
      toast.error(t("Check Your Email To Activate Your Account"));
      if (loginFormS.data) {
        delete loginFormS.data.password;
      }
      delete loginFormS.submitting;
      dispatch({
        type: dispatchType,
        payload: loginFormS,
      });
    },
    else: (data?: any): void => {
      if (data) {
        toast.error(t(data.Msg));
      }

      if (loginFormS.data) {
        delete loginFormS.data.password;
      }
      delete loginFormS.submitting;
      dispatch({
        type: dispatchType,
        payload: loginFormS,
      });
    },
  };

  const handleSignUp = () => {
    const SignUp = React.lazy<any>(() => import("./SignUp"));
    dispatch({
      type: "dialog",
      payload: {
        show: true,
        type: "signUpForm",
        component: () => (
          <React.Suspense fallback="">
            <SignUp />
          </React.Suspense>
        ),
      },
    });
  };

  const formData: IForm = {
    dispatchType,
    fields,
    access: "guest/login",
    respHandler,
    className: "login-form",
    title: "Login to Your Account",
    submitButtonHtml: "Login",
    isViaDialog: true,
    backButton: false,
  };

  return (
    <section className="mt-5 p-3">
      <button
        type="button"
        className="close"
        onClick={() => {
          dispatch({ type: "dialogClose" });
        }}
      >
        <span
          onClick={() => {
            dispatch({ type: "dialogClose" });
          }}
        >
          &times;
        </span>
      </button>

      <Form {...formData} />

      {/* <div className="row justify-content-center">
        <p className="href">{t("Forget password?")}</p>
      </div> */}
      <div className="row justify-content-center">
        <p className="href" onClick={handleSignUp}>
          {t("Sign up now")}
        </p>
      </div>
    </section>
  );
}
