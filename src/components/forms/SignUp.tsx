import React from "react";

import { Store } from "../../Store";
import { IForm, IFormField } from "../../interfaces";
import Form from "../common/Form";

export default function SignUp(): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const dispatchType = "signUpForm";

  const respHandler = {
    "200": (data?: any): void => {
      if (data) {
        const SignUpMsg = React.lazy<any>(() => import("../dialogs/SignUpMsg"));
        dispatch({
          type: "dialog",
          payload: {
            show: true,
            type: "signUpMsgForm",
            component: () => (
              <React.Suspense fallback="">
                <SignUpMsg email={state[dispatchType].data.email} />
              </React.Suspense>
            ),
          },
        });
      }
    },
  };

  const fields: Array<IFormField> = [
    {
      name: "email",
      label: "Email address",
      schema: "email;required",
      className: "col-sm-12 floating-label",
    },
    {
      name: "name_",
      label: "Name",
      className: "col-sm-12 floating-label",
    },
    {
      name: "password",
      type: "password",
      label: "Password",
      schema: "required",
      className: "col-sm-12 floating-label",
    },
    {
      name: "confirmed_password",
      label: "Confirmed password",
      type: "password",
      schema: "required;match[password]",
      className: "col-sm-12 floating-label",
    },
  ];

  const formData: IForm = {
    dispatchType,
    fields,
    access: "guest",
    className: "sign-up-form",
    title: "Sign Up",
    isViaDialog: true,
    backButton: false,
    respHandler,
  };

  return (
    <section className="mt-5 p-3">
      <button
        type="button"
        className="close"
        onClick={() => {
          dispatch({ type: "dialogClose" });
        }}
      >
        <span>&times;</span>
      </button>

      <Form {...formData} />
    </section>
  );
}
