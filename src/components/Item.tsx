import React from "react";
import { toast } from "react-toastify";

import { Store } from "../Store";
import { TObject } from "../interfaces";
import { getEndpoint } from "../utils/common";
import http from "../utils/http";
import hook from "../utils/hook";
import t from "../utils/translation";

export default function Item(): JSX.Element {
  const { state, dispatch } = React.useContext(Store);
  const dispatchType = "itemArr";

  React.useEffect(() => {
    initState();
    return () => dispatch({ type: "delete", payload: dispatchType });
  }, [dispatch]);

  const initState = () => {
    setState();
  };

  const setState = async (): Promise<void> => {
    dispatch({ type: "dialogMask" });
    const respHandler = {
      "200": (data: any) => {
        dispatch({ type: dispatchType, payload: data });
      },
    };

    const resp = await http.get(`${getEndpoint()}item`);
    dispatch({ type: "dialogClose" });

    hook.handleResp(dispatch, resp, respHandler);
  };

  return (
    <div className="mt-5 mb-5 ">
      <div className="row text-center">
        {state["itemArr"] &&
          state["itemArr"].map((certain: TObject) => {
            if (certain.photo) {
              return (
                <div
                  className="col-lg-3 col-md-4 col-6"
                  key={certain.id}
                  onClick={() => toast.warn(t("Developing") + "...")}
                >
                  <div className="mb-4">
                    <img
                      className="img-fluid img-thumbnail rounded opacity-9"
                      src={`${getEndpoint()}${certain.path}/${
                        certain.thumb ? "thumb/" : ""
                      }${certain.photo}`}
                    />
                  </div>
                </div>
              );
            }
          })}
      </div>
    </div>
  );
}
