import React from "react";

import { TObject } from "../interfaces";
import NavHeader from "./navigations/Header";
export default function Frame({ children }: TObject): JSX.Element {
  return (
    <>
      <NavHeader />
      <div className="container-fluid">
        <div className="row">
          <div className="col">{children}</div>
        </div>
      </div>
    </>
  );
}
