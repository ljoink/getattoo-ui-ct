import React from "react";

import Banner from "../HomeBanner";
import Item from "../Item";

import "../../style/home.scss";

export default function Home(): JSX.Element {
  return (
    <>
      <Banner />
      <Item />
    </>
  );
}
