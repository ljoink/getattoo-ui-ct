import React from "react";
import { useHistory, useParams } from "react-router-dom";
import { toast } from "react-toastify";

import { Store } from "../../Store";
import { TObject } from "../../interfaces";
import { getEndpoint, getLangUrl } from "../../utils/common";
import http from "../../utils/http";
import hook from "../../utils/hook";
import t from "../../utils/translation";

export default function Activate(): JSX.Element {
  const { dispatch } = React.useContext(Store);

  const params: TObject = useParams();
  React.useEffect(() => {
    if (params.id && params.code) {
      setState();
    }
  }, []);

  const history = useHistory();
  const setState = async (): Promise<void> => {
    const resp = await http.get(
      `${getEndpoint()}guest/activate/${params.id}/${params.code}`
    );
    dispatch({ type: "dialogClose" });

    dispatch({ type: "dialogMask" });
    const respHandler = {
      "200": () => {
        toast.success(t("Activated successfully"));
      },
      finally: () => {
        history.push(getLangUrl());
      },
    };

    hook.handleResp(dispatch, resp, respHandler);
  };

  return <></>;
}
