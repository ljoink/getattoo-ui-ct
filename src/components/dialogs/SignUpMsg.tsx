import React from "react";

import { Store } from "../../Store";
import { TObject } from "../../interfaces";
import t from "../../utils/translation";

export default function SignUpMsg({ email }: TObject): JSX.Element {
  const { dispatch } = React.useContext(Store);
  return (
    <section className="mt-5 p-3 sign-up-msg">
      <button
        type="button"
        className="close"
        onClick={() => {
          dispatch({ type: "dialogClose" });
        }}
      >
        <span>&times;</span>
      </button>
      <h3>{t("Almost done")}...</h3>
      <p>
        {t(
          "We've sent an email to %%email%%. Open it up to activate your account.",
          { email }
        )}
      </p>
    </section>
  );
}
