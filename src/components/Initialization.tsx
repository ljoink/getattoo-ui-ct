import React from "react";

import { useHistory } from "react-router-dom";

import { Store } from "../Store";
import http from "../utils/http";
import {
  getBaseUrl,
  getEndpoint,
  getStorage,
  setStorage,
} from "../utils/common";

export default function Initialization(): null {
  const { state, dispatch } = React.useContext(Store);
  const {
    initialled: initialledS,
    languageArr: languageArrS,
    siteInfo: siteInfoS,
  } = state;

  React.useEffect(() => {
    if (!initialledS) {
      if (languageArrS.length === 0 && Object.keys(siteInfoS).length === 0) {
        setLanguageArr();
        setSiteInfo();
      }

      if (languageArrS.length !== 0 && Object.keys(siteInfoS).length !== 0) {
        setLangStorage();
        setInitialled();
      }
    } else {
      document.title = siteInfoS[`name_${getStorage("Lang")}`];
      modifyUrl();
    }
  });

  const history = useHistory();

  const langIndex = getBaseUrl() ? 2 : 1;
  let sagments = history.location.pathname.split("/") as Array<string>;

  const urlPushLang = (): void => {
    sagments.splice(langIndex, 0, getStorage("Lang"));
    const path = `${sagments.join("/")}${history.location.search}`;
    history.push(
      path.charAt(path.length - 1) === "/" ? path.slice(0, -1) : path
    );
  };

  const modifyUrl = () => {
    if (sagments[langIndex]) {
      if (sagments[langIndex] !== getStorage("Lang")) {
        const langCodeArr = languageArrS.map((current) => current.code);
        //delete all url.lang if url.lang !== lang
        sagments = sagments.filter((current) => !langCodeArr.includes(current));
        urlPushLang();
      }
    } else {
      urlPushLang();
    }
  };

  const setLanguageArr = async (): Promise<void> => {
    const { data } = await http.get(getEndpoint() + "language");
    dispatch({
      type: "languageArr",
      payload: data,
    });
  };

  const setSiteInfo = async (): Promise<void> => {
    const { data } = await http.get(getEndpoint() + "site-info/1");
    dispatch({
      type: "siteInfo",
      payload: data,
    });
  };

  const setLangStorage = (): void => {
    let sagmentLanguage;
    if (sagments[langIndex]) {
      sagmentLanguage = languageArrS.find(
        (current) => current.code === sagments[langIndex]
      );
    }

    if (sagmentLanguage && sagmentLanguage.hasOwnProperty("code")) {
      setStorage("Lang", sagmentLanguage.code);
    } else if (!getStorage("Lang")) {
      const defaultLanguage = languageArrS.filter(
        (current) => current.is_default === "true"
      );
      setStorage(
        "Lang",
        defaultLanguage.length === 0 ? "en" : defaultLanguage[0].code
      );
    }
  };

  const setInitialled = (): void => {
    dispatch({
      type: "initialled",
      payload: true,
    });
  };

  return null;
}
