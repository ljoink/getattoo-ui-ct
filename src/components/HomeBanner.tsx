import React from "react";

import { Store } from "../Store";
import Gallery from "./common/Gallery";
import { getEndpoint } from "../utils/common";
import http from "../utils/http";
import hook from "../utils/hook";

export default function HomeBanner(): JSX.Element {
  const { dispatch } = React.useContext(Store);
  const dispatchType = "homeBannerArr";

  React.useEffect(() => {
    const setState = async (): Promise<void> => {
      dispatch({ type: "dialogMask" });
      const respHandler = {
        "200": (data: any) => {
          dispatch({ type: dispatchType, payload: data });
        },
      };

      const resp = await http.get(`${getEndpoint()}home-banner`);
      dispatch({ type: "dialogClose" });

      hook.handleResp(dispatch, resp, respHandler);
    };

    setState();

    return () => dispatch({ type: "delete", payload: dispatchType });
  }, [dispatch]);

  return <Gallery className="opacity-9" dispatchType={dispatchType} />;
}
