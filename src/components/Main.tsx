import React from "react";
import { Switch, Route } from "react-router-dom";
import { ToastContainer } from "react-toastify";

import "react-toastify/dist/ReactToastify.css";

import { Store } from "../Store";
import { AuthRoute } from "./common/AuthRoute";
import Dialog from "./common/Dialog";
import ScrollTopButton from "./common/ScrollTopButton";

import { getLangUrl } from "../utils/common";
import Frame from "./Frame";

const Home = React.lazy<any>(() => import("./pages/Home"));
const Activate = React.lazy<any>(() => import("./pages/Activate"));

export default function Router() {
  const { state } = React.useContext(Store);
  const { initialled: initialledS } = state;
  const langUrl = getLangUrl();

  const NoMatch = () => <p style={{ color: "#fff" }}>No Match</p>;

  if (initialledS) {
    return (
      <Frame>
        <Switch>
          <AuthRoute exact path={`${langUrl}`} lazy={Home} />
          <AuthRoute
            exact
            path={`${langUrl}/activate/:id/:code`}
            lazy={Activate}
          />

          <Route path="*" component={NoMatch} />
        </Switch>

        <ToastContainer style={{ zIndex: 99999 }} />
        <ScrollTopButton />
        <Dialog />
      </Frame>
    );
  } else {
    return <h3>Loading...</h3>;
  }
}
